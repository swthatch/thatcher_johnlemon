﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerMovement : MonoBehaviour
{
    public float turnSpeed = 20f;
    public float speed;

    Rigidbody m_Rigidbody;
    AudioSource m_AudioSource;
    Animator m_Animator;
    Vector3 m_Movement;
    Quaternion m_Rotation = Quaternion.identity;
    int score;
    public bool big;
    public Text scoretext;
    public AudioSource Enemysfx;

    // Start is called before the first frame update
    void Start()
    {
        m_Animator = GetComponent<Animator>();
        m_AudioSource = GetComponent<AudioSource>();
        m_Rigidbody = GetComponent<Rigidbody>();
        setscoretext();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        float horizontal = Input.GetAxis("Horizontal");
        float vertical = Input.GetAxis("Vertical");

        m_Movement.Set(horizontal, 0f, vertical);
        m_Movement.Normalize();

        bool hasHorizontalInput = !Mathf.Approximately(horizontal, 0f);
        bool hasVerticalInput = !Mathf.Approximately(vertical, 0f);
        bool isWalking = hasHorizontalInput || hasVerticalInput;
        m_Animator.SetBool("IsWalking", isWalking);
        if (isWalking)
        {
            if (!m_AudioSource.isPlaying)
            {
                m_AudioSource.Play();
            }
        }
        else
        {
            m_AudioSource.Stop();
        }

        Vector3 desiredForward = Vector3.RotateTowards(transform.forward, m_Movement, turnSpeed * Time.deltaTime, 0f);
        m_Rotation = Quaternion.LookRotation(desiredForward);
    }
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.LeftShift))
        {
            print("shift key is being pressed");
            speed =2f;
            m_Animator.speed = speed;

        }
        if (Input.GetKeyUp(KeyCode.LeftShift))
        {
            speed = 1f;
            m_Animator.speed = speed;
        }
    }
    void OnAnimatorMove()
    {
        m_Rigidbody.MovePosition(m_Rigidbody.position + m_Movement * m_Animator.deltaPosition.magnitude);
        m_Rigidbody.MoveRotation(m_Rotation);
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Shrink"))
        {
            if (Random.value > .5f)
            {
                transform.localScale = Vector3.one * 2f;
                big = true;
                Invoke("ReturnToFullSize", 5f);

                Destroy(other.gameObject);

            }
            else
            {
                transform.localScale = Vector3.one * .5f;
                Invoke("ReturnToFullSize", 5f);

                Destroy(other.gameObject);
 

            }

        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Gargoyle")&& big)
        {
            Destroy(collision.gameObject);
            PlayEnemysfx();
            score += 10;
            setscoretext();

        }
        else if(collision.gameObject.CompareTag("Ghost")&& big)
        {
            Destroy(collision.gameObject);
            PlayEnemysfx();
            score += 50;
            setscoretext();
        }
    }

    void setscoretext()
    {
        scoretext.text = "Score:"+ score.ToString();

    }

    void ReturnToFullSize()
    {
        transform.localScale = Vector3.one;
        big = false;
    }
    public void PlayEnemysfx()
    {
        print("CALLED PLAY!");
        Enemysfx.Play();
    }
}
